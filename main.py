from sys import argv
from lyricgraph.models.album import Album
from lyricgraph.transformations.sources.lyric import DEFAULT_LYRIC_SOURCE


def main():
    if(len(argv) is not 2):
        print("{} songpath".format(argv[0]))
        exit(1)

    a = Album(from_path=argv[1])
    lyric = a.get_songs()[0].get_lyrics()[0]

    print(DEFAULT_LYRIC_SOURCE)
    for x in lyric.get_samples(DEFAULT_LYRIC_SOURCE):
        print(x)

if __name__ == '__main__':
    main()
