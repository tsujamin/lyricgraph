from lyricgraph.models.lyric import Lyric

REQUIRED_SONG_KEYS = set(["album", "artist", "title", "lyrics"])


class Song(object):
    """docstring for Song."""
    def __init__(self, json_dict, parent_album):
        """
        TODO docstring
        """
        super(Song, self).__init__()

        # Check keys of JSON object
        present_keys = set(json_dict.keys())
        if(not present_keys.issuperset(REQUIRED_SONG_KEYS)):
            missing_keys = present_keys.difference(REQUIRED_SONG_KEYS)

            raise KeyError("Song missing keys \"{}\"".format(
                ", ".join(missing_keys)))

        # Check types of JSON object
        if(type(json_dict["album"]) is not str):
            raise TypeError("Song key \"album\" is {}, expected str".format(
                type(json_dict["album"])))
        elif(type(json_dict["artist"]) is not str):
            raise TypeError("Song key \"artist\" is {}, expected str".format(
                type(json_dict["artist"])))
        elif(type(json_dict["title"]) is not str):
            raise TypeError("Song key \"title\" is {}, expected str".format(
                type(json_dict["title"])))
        elif(type(json_dict["lyrics"]) is not list):
            raise TypeError("Song key \"lyrics\" is {}, expected list".format(
                type(json_dict["lyrics"])))
        else:
            for line in json_dict["lyrics"]:
                if(type(line) is not str):
                    raise TypeError("Song line \"{}\" is {}, expected str".format(
                        line, type(line)))

        # JSON dict is valid, assign fields to song
        self.parent_album = parent_album
        self.title = json_dict["title"]
        self.lyrics = list(map(lambda line: Lyric(line, self),
                                json_dict["lyrics"]))
        self.dict = json_dict

    def get_dict(self):
        """
        x.get_dict() -> dict. Returns the dict loaded from the song's JSON file.
        """
        return self.dict

    def get_artist(self):
        """
        x.get_artist() -> str. Return the name of the parent album's artist.
        """
        return self.parent_album.get_artist()

    def get_album(self):
        """
        x.get_album() -> str. Return the name of the parent album.
        """
        return self.parent_album.get_album()

    def get_title(self):
        """
        x.get_title() -> str. Return the title of the song.
        """
        return self.title

    def get_lyrics(self):
        """
        x.get_lyrics() -> list. Return a list of the Song's child Lyric objects.
        """
        return list(self.lyrics)

    def get_ngrams(self, n, **kwargs):
        # TODO pass kwargs to each lyric too
        pass

    def __str__(self):
        return "{}, {}, {}".format(self.get_artist(),
                                    self.get_album(),
                                    self.title)

    def __repr__(self):
        return "<Song: {}>".format(str(self))
