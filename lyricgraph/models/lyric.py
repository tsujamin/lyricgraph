# TODO document


class Lyric(object):
    """docstring for Lyric."""
    def __init__(self, line, parent_song):
        """
        TODO
        """
        super(Lyric, self).__init__()

        self.line = line
        self.parent_song = parent_song

    def get_lyric(self):
        return self.line

    def get_parent_song(self):
        return self.parent_song

    def get_samples(self, source, **kwargs):
        source.reset()
        source.set_root_source([self.line])

        # Convert the result of the final source in the pipeline to a list
        return list(source.sink())

    def __str__(self):
        return self.line

    def __repr__(self):
        return "<Lyric: {}, \"{}\">".format(str(self.get_parent_song()),
                                        self.line)
