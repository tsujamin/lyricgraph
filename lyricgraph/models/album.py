from glob import glob
from json.decoder import JSONDecodeError
import json
from lyricgraph.models.song import Song


def load_songs_from_path(path):
    """
    load_songs_from_path() -> list. Load and parse all JSON songs in the path.
    """
    songs = []
    # We expect all songs to be named N.json
    # TODO order list by song numner
    for file_name in glob(path + "/*.json"):
        with open(file_name) as f:
            try:
                songs.append(json.loads(f.read()))
            except JSONDecodeError as e:
                # Standard JSONDecodeError doesn't print which file failed.
                # Print our own error too
                print("Failed to decode {}".format(file_name))
                raise e

    return songs


class Album(object):
    """docstring for Album."""

    def __init__(self, from_path="."):
        """
        __init__() Initialise an album from the present working directory

        __init__(from_path='path/to/album') Initialise an album using from_path

        Albums are defined as a directory of JSON formated files on the
        filesystem, each conforming to the format:
        {
            title: "Song Title",
            artist: "Artist",
            album: "Album",
            lyrics: [
                "Line one of song",
                "Line two of song",
                "",
                ""Line three of song"
            ]
        }
        """
        super(Album, self).__init__()
        # Load the songs from the provided path or current directory
        # TODO check length of returned array
        self.songs = list(map(lambda song_dict: Song(song_dict, self),
                              load_songs_from_path(from_path)))

        # Use the details of the first song as album details
        self.artist = self.songs[0].get_dict()["artist"]
        self.album = self.songs[0].get_dict()["album"]

    def get_artist(self):
        """
        x.get_artist() -> str. Return the name of the album's artist.
        """
        return self.artist

    def get_album(self):
        """
        x.get_album() -> str. Return the name of the album.
        """
        return self.album

    def get_songs(self):
        """
        x.get_songs() -> list. Return a list of the Album's Song objects
        """
        # Return a copy of the list
        return list(self.songs)

    def get_song_names(self):
        """
        x.get_song_names() -> list. Return a list of the Album's song names.
        """
        return list(map(lambda song: song.get_title(), self.songs))

    def __str__(self):
        return "{}, {}, [{}]".format(self.artist,
                                    self.album,
                                    ", ".join(self.get_song_names()))

    def __repr__(self):
        return "<Album: {}>".format(str(self))
