from lyricgraph.transformations.sources import Source


class LowerCaseSource(Source):
    """
    LowerCaseSource yields the parent_source's items in lowercase

    Example:
    lower = LowerCaseSource(parent_source=["HELLO World"])
    print(list(lower.sink()))
    # ["hello world"]
    """
    def __init__(self, parent_source=None):
        """
        x.__init__(parent_source=p) Initialise a LowerCaseSource
        """
        super(LowerCaseSource, self).__init__(parent_source=parent_source)

    def process(self, item):
        """
        x.process(item) -> iter. Yield the lowercase form of item.
        """
        yield item.lower()
