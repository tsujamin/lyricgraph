import string
from lyricgraph.transformations.sources import Source


class StripPunctuationSource(Source):
    """
    StripPunctuationSource strips punctuation from the items yielded by its
    parent source

    Example:
    stirp = StripPunctuationSource(parent_source=["Hello, World!"])
    print(list(strip.sink()))
    # ["Hello World"]
    """
    def __init__(self, parent_source=None):
        """
        x.__init__(parent_source=p) Initialise a StripPunctuationSource
        """
        super(StripPunctuationSource, self).__init__(parent_source=parent_source)

    def process(self, item):
        """
        x.process(item) -> iter. Yields item stripped of punctuation.

        Uses the string.punctuation list.
        """
        yield "".join([c for c in item if c not in string.punctuation])
