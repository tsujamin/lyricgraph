from lyricgraph.transformations.sources import Source


class NgramSource(Source):
    """
    NgramSource divides the items provided by it's parent source into N-grams,
    where N is provided in the __init__ or set_n methods.

    Example:
    ngram = NgramSource(3, parent_source=["Do Re Mi Fa So"])
    print(list(ngram.sink()))
    # "["Do Re Mi", "Re Mi Fa", "Mi Fa So"]"
    """
    def __init__(self, n, parent_source=None):
        """
        x.__init__(3, parent_source=p) Initialise an 3-gram Source
        """
        super(NgramSource, self).__init__(parent_source=parent_source)
        self.n = n

    def set_n(self, n):
        """
        x.set_n(3) Set the Source to be an 3-gram etc.
        """
        self.n = n

    def process(self, line):
        """
        x.process(item) -> iter. Returns an iterable of item's ' ' split N-grams

        See Source.process()
        """
        ngrams = []
        # Split the line of text into constituent words
        line_items = line.split(" ")

        # Make the ngrams
        if self.n > len(line_items) or self.n < 1:
            yield line
        else:
            for i in range(len(line_items) - self.n + 1):
                yield " ".join(line_items[i:i + self.n])

    def get_source_name(self):
        return "{}gramSource".format(self.n)
