from .ngram import NgramSource
from .strip import StripPunctuationSource
from .stem import StemmingSource
from .lowercase import LowerCaseSource

_default_lowercase_source = LowerCaseSource()
_default_strip_source = StripPunctuationSource(parent_source=_default_lowercase_source)
_default_stem_source = StemmingSource(parent_source=_default_strip_source)
_default_ngram_source = NgramSource(3, parent_source=_default_stem_source)

DEFAULT_LYRIC_SOURCE = _default_ngram_source
