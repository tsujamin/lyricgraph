from stemming.porter2 import stem
from lyricgraph.transformations.sources import Source


class StemmingSource(Source):
    """
    StemmingSource Stems the words in the parent_source using the porter 2
    stemming algorithm.

    Example:
    stem = StemmingSource(parent_source=["Stemmed Stemming"])
    print(list(stem.sink()))
    # ["Stem Stem"]
    """
    def __init__(self, parent_source=None):
        """
        x.__init__(parent_source=p) Initialise a StemmingSource
        """
        super(StemmingSource, self).__init__(parent_source=parent_source)

    def process(self, item):
        """
        x.process(item) -> iter. Yields item stemmed using the porter2 stemmer.
        """
        yield " ".join(map(lambda word: stem(word), item.split(" ")))
